import quotesData from '../data/quotes.json';
import { Quote } from '../interfaces/quote';

class QuoteRA
{
    getQuotes() : Array<Quote>
    {
        return quotesData;
    }

    getCharacterQuotes(characterName : string) : Array<Quote>
    {
        return quotesData.reduce((acc : Array<Quote>, quote : Quote) =>
        {
            if(quote.name.toLowerCase().indexOf(characterName.toLowerCase()) > -1)
            {
                acc.push(quote);
            }
            return acc;
        }, []);
    }

    getCharacters() : Array<string>
    {
        return quotesData.reduce((acc : Array <string>, quote : Quote) =>
        {
            if(acc.indexOf(quote.name) === -1)
            {
                acc.push(quote.name);
            }
            return acc;
        }, []);
    }
}

export default new QuoteRA();

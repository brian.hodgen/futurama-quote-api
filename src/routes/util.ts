import express from 'express';

const router = express.Router();

router.get('/ping', (_req, res) =>
{
    res.end('pong');
});

router.get('/echo', (_req, res) =>
{
    res.end('No value to echo back!');
});

router.get('/echo/:value', (req, res) =>
{
    res.end(req.params.value);
});

export default router;

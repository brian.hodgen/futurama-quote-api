import express from 'express';
import QuoteManager from '../managers/quoteManager';

const router = express.Router();

router.get('/random', (_req, res) =>
{
    res.end(QuoteManager.getRandomQuote());
});

router.get('/characters', (_req, res) =>
{
    res.json(QuoteManager.getCharacterList());
    res.end();
});

router.get('/random/:name', (req, res) =>
{
    res.end(QuoteManager.getRandomQuote(req.params.name));
});

router.post('/', (req, res) =>
{
    if(req.body.sort)
    {
        if(req.body.sort !== 'name' && req.body.sort !== 'quote')
        {
            res.end(`${ req.body.sort } is not a valid sort option, please use "name" or "quote"`);
            return;
        }
    }
    res.json(QuoteManager.getQuoteObjects(req.body.filter, req.body.sort));
    res.end();
});

export default router;

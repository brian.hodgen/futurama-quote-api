import express from 'express';
import bodyParser from 'body-parser';

import UtilRouter from './routes/util';
import QuoteRouter from './routes/quotes';

const app = express();
app.use(bodyParser.json());

app.use('/util', UtilRouter);
app.use('/quotes', QuoteRouter);

app.listen(1337);
console.log('Listening on port 1337');

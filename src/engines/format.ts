import _ from 'lodash';
import { Quote } from '../interfaces/quote';

export function formatQuoteString(quote : Quote) : string
{
    return `"${ quote.quote }" -${ quote.name }`;
}

export function sortStrings(list : Array <string>) : Array<string>
{
    return list.sort();
}

export function sortCollection(sortType : string, collection : Array<Quote>) : Array<Quote>
{
    switch (sortType)
    {
        case 'name':
            return _.sortBy(collection, [ 'name', 'quote' ]);
        case 'quote':
            return _.sortBy(collection, [ 'quote', 'name' ]);
        default:
            return collection;
    }
}

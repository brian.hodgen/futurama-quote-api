import _ from 'lodash';
import QuoteRA from '../resource-access/quoteRA';
import { formatQuoteString, sortStrings, sortCollection } from '../engines/format';
import { Quote } from '../interfaces/quote';

class QuoteManager
{
    private _getFilteredQuotes(filter ?: string | Array<string>) : Array<Quote>
    {
        let quotes : Array<Quote> = [];
        if(filter)
        {
            const aFilter : Array<string> = _.castArray(filter);
            const allQuotes = aFilter.map((filterString : string) =>
            {
                return QuoteRA.getCharacterQuotes(filterString);
            });

            quotes = _.flattenDeep(allQuotes);
        }
        else
        {
            quotes = QuoteRA.getQuotes();
        }
        return quotes;
    }

    getRandomQuote(characterName ?: string) : string
    {
        const quotes : Array<Quote> = this._getFilteredQuotes(characterName);
        if(quotes.length > 0)
        {
            const randomQuote = _.sample(quotes);
            return formatQuoteString(randomQuote);
        }
        return `No Quotes for ${ characterName } were found.`;
    }

    getQuoteObjects(characterName ? : string, sort ?: string) : Array<Quote>
    {
        const quotes : Array<Quote> = this._getFilteredQuotes(characterName);
        if(sort)
        {
            return sortCollection(sort, quotes);
        }
        return quotes;
    }

    getCharacterList() : Array<string>
    {
        const characterList = QuoteRA.getCharacters();
        return sortStrings(characterList);
    }
}

export default new QuoteManager();
